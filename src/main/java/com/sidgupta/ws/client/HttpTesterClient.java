package com.sidgupta.ws.client;

import com.sidgupta.ws.http.HttpResponseStatus;
import com.sidgupta.ws.utils.Constants;
import com.sidgupta.ws.utils.ExceptionExtensions;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by sidgupta on 5/27/15.
 */
public class HttpTesterClient
{
    public static void main(final String[] args) throws Exception
    {
        //disable apache http client logging
        Logger.getLogger("org.apache.http").setLevel(Level.OFF);

        final String serverAddress = "http://127.0.0.1:8888/";
        final String resourceToRequest = "sometext.txt";

        final String resourceAddress = serverAddress + resourceToRequest;

        final int numConcurrentRequests = 20;
        final int numRequestsPerThread = 100;

        final ExecutorService executorService = Executors.newFixedThreadPool(numConcurrentRequests);

        for(int i = 0; i < numConcurrentRequests; i++)
        {
            executorService.submit(
                    new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            final CloseableHttpClient httpClient = HttpClients.createDefault();

                            for(int i = 0; i < numRequestsPerThread; i++)
                            {
                                try
                                {
                                    final HttpGet httpGet = new HttpGet(resourceAddress);
                                    final HttpResponse response = httpClient.execute(httpGet);

                                    if(response.getStatusLine().getStatusCode() != HttpResponseStatus.OK.statusCode)
                                    {
                                        System.out.println(
                                                String.format(
                                                        "[%s] request failed with error response [%s] - [%s], entity [%s]",
                                                        Thread.currentThread().getName(),
                                                        response.getStatusLine().getStatusCode(),
                                                        response.getStatusLine().getReasonPhrase(),
                                                        (response.getEntity() == null)?
                                                                Constants.NULL_STRING : EntityUtils.toString(response.getEntity())));
                                    }
                                    else
                                    {
                                        EntityUtils.toString(response.getEntity());
                                    }
                                }
                                catch (final Exception ex)
                                {
                                    System.out.println(
                                            String.format(
                                                    "[%s] caught exception. continuing. Exception %s",
                                                    Thread.currentThread().getName(),
                                                    ExceptionExtensions.getExceptionMessageWithStackTraceFor(ex)));
                                }

                                if(i%10 == 0)
                                    System.out.println(
                                            String.format(
                                                    "[%s] completed requests [%d]",
                                                    Thread.currentThread().getName(),
                                                    i));
                            }
                        }
                    });
        }

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.HOURS);

        executorService.shutdownNow();
    }
}
