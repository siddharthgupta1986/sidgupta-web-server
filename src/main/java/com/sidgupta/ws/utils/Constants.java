package com.sidgupta.ws.utils;

import java.io.File;

/**
 * Created by siddharth on 5/27/15.
 */
public class Constants
{
    public static final String PERIOD = ".";
    public static final String QUESTION_MARK = "?";
    public static final String EQUALS = "=";
    public static final String AMPERSAND = "&";
    public static final String COLON = ":";
    public static final String EMPTY_STRING = "";
    public static final String NULL_STRING = "null";
    public static final String PIPE = "|";
    public static final String TRAVERSE_UP = "..";
    public static final String FORWARD_SLASH = "/";

    public static final String CONTENT_LENGTH_HEADER_KEY = "Content-Length";
    public static final String CONTENT_TYPE_HEADER_KEY = "Content-Type";
    public static final String CHARSET_HEADER_KEY = "charset";
    public static final String DATE_HEADER_KEY = "Date";
    public static final String SERVER_NAME_HEADER_KEY = "Server";

    public static final String SERVER_NAME_HEADER_VALUE = "sidgupta-server";

    public static final String TEXT_PLAIN_MIME_TYPE = "text/plain";

    public static final String HTTP_VERSION = "HTTP/1.1";
}
