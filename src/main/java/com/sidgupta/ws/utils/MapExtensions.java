package com.sidgupta.ws.utils;

import java.util.Map;

/**
 * Created by sidgupta on 5/26/15.
 */
public class MapExtensions
{
    public static <K,V> boolean isNullOrEmpty(final Map<K,V> inputMap)
    {
        if(inputMap == null || inputMap.isEmpty())
            return true;

        return false;
    }

    public static <K,V> boolean isNotNullOrEmpty(final Map<K,V> inputMap)
    {
        return !(isNullOrEmpty(inputMap));
    }
}
