package com.sidgupta.ws.utils;

/**
 * Created by sidgupta on 5/26/15.
 */
public class Ensure
{
    public static <T> T notNull(final T input, final String objectName)
    {
        if(input == null)
            throw new IllegalArgumentException(
                    String.format(
                            "expected [%s] to be not null, but found null",
                            objectName));

        return input;
    }

    public static String notNullOrEmpty(final String input, final String objectName)
    {
        if(StringExtensions.isNullOrEmpty(input))
            throw new IllegalArgumentException(
                    String.format(
                            "expected [%s] to be not null or empty",
                            objectName));

        return input;
    }
}
