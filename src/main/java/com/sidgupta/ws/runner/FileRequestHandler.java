package com.sidgupta.ws.runner;

import com.sidgupta.ws.http.HttpRequest;
import com.sidgupta.ws.http.HttpRequestHandler;
import com.sidgupta.ws.http.HttpResponse;
import com.sidgupta.ws.http.HttpResponseStatus;
import com.sidgupta.ws.utils.Constants;
import com.sidgupta.ws.utils.Ensure;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

/**
 * Created by sidgupta on 5/27/15.
 */
public class  FileRequestHandler implements HttpRequestHandler
{
    public final String rootDirectoryPath;

    public FileRequestHandler(final String rootDirectoryPath)
    {
        this.rootDirectoryPath = Ensure.notNullOrEmpty(rootDirectoryPath, "rootDirectoryPath");
        final File rootDirectoryFile = new File(rootDirectoryPath);

        if(!rootDirectoryFile.canRead() || !rootDirectoryFile.exists() || !rootDirectoryFile.isDirectory())
        {
            throw new IllegalArgumentException(
                    String.format(
                            "invalid root directory [%s] for server",
                            rootDirectoryPath));
        }
    }

    @Override
    public HttpResponse apply(final HttpRequest request)
    {
        switch(request.requestMethod)
        {
            case GET:
            {
                try
                {
                    if(request.resource.contains(Constants.TRAVERSE_UP)||
                            request.resource.contains(Constants.COLON)||
                            request.resource.contains(Constants.PIPE))
                        return new HttpResponse(HttpResponseStatus.NOT_FOUND, "requested resource not found");

                    Path resourcePath = Paths.get(rootDirectoryPath, request.resource);
                    if(request.resource.equals(Constants.FORWARD_SLASH))
                        resourcePath = Paths.get(resourcePath.toString(), "index.html");

                    final File resourceFile = resourcePath.toFile();

                    if(!resourceFile.exists())
                        return new HttpResponse(HttpResponseStatus.NOT_FOUND, "requested resource not found");

                    if(!resourceFile.canRead())
                        return new HttpResponse(HttpResponseStatus.FORBIDDEN, "do not have appropriate permissions");

                    if(!resourceFile.isFile())
                        return new HttpResponse(HttpResponseStatus.BAD_REQUEST, "inappropriate resource requested");

                    final long resourceFileLength = resourceFile.length();
                    final String resourceMimeType =
                            MimetypesFileTypeMap.getDefaultFileTypeMap().getContentType(resourceFile);

                    return new HttpResponse(
                            HttpResponseStatus.OK,
                            new HashMap<String, String>()
                            {{
                                put(Constants.CONTENT_LENGTH_HEADER_KEY, String.valueOf(resourceFileLength));
                            }},
                            resourceMimeType,
                            new FileInputStream(resourceFile));
                }
                catch(final Exception ex)
                {
                    return new HttpResponse(
                            HttpResponseStatus.INTERNAL_SERVER_ERROR,
                            ex.getMessage());
                }
            }
            default:
            {
                return new HttpResponse(
                        HttpResponseStatus.NOT_IMPLEMENTED,
                        String.format(
                                "request method [%s] is not yet implemented",
                                request.requestMethod.requestMethodName));
            }
        }
    }
}
