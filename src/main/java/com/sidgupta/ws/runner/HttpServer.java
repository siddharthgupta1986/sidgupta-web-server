package com.sidgupta.ws.runner;

import com.sidgupta.ws.simplewebserver.SimpleWebServer;
import com.sidgupta.ws.utils.Ensure;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;

/**
 * Created by sidgupta on 5/26/15.
 */
public class HttpServer
{
    private static final Logger logger = Logger.getLogger(HttpServer.class);
    private static final long maxWaitForServerToTerminate = 2 * 60 * 1000; // 2 minutes

    private static Options getServerOptions()
    {
        final Options options = new Options();

        options.addOption(
                Option.builder("p").longOpt("server-port").type(Integer.class).
                        hasArg(true).desc("port number (int) on which server will listen").required(true).build());

        options.addOption(
                Option.builder("t").longOpt("num-server-threads").type(Integer.class).
                        hasArg(true).desc("number (int) of server worker threads").required(true).build());

        options.addOption(
                Option.builder("d").longOpt("server-directory").type(String.class).
                        hasArg(true).desc("directory (string) from which the files will be served").required(true)
                        .build());

        return options;
    }

    private static void printHelp(final Options serverOptions)
    {
        new HelpFormatter().printHelp("HttpServer", serverOptions);
    }

    public static void main(final String[] args) throws Exception
    {
        int portNumber;
        int numRequestHandlingThreads;
        String serverDirectory;

        final Options serverOptions = getServerOptions();
        try
        {
            final CommandLine commandLine = new DefaultParser().parse(serverOptions, args);

            portNumber = Integer.parseInt(commandLine.getOptionValue("p"));
            numRequestHandlingThreads = Integer.parseInt(commandLine.getOptionValue("t"));
            serverDirectory = Ensure.notNullOrEmpty(commandLine.getOptionValue("d"), "serverDirectory");
        }
        catch(final ParseException | IllegalArgumentException ex)
        {
            System.out.println("Incorrect option usage. Please read below.");
            printHelp(serverOptions);

            return;
        }

        final SimpleWebServer webServer =
                new SimpleWebServer(
                        portNumber,
                        numRequestHandlingThreads,
                        new FileRequestHandler(serverDirectory));

        final Thread serverThread = new Thread(webServer);
        serverThread.start();

        // shutdown hook for graceful server termination
        Runtime.getRuntime().addShutdownHook(
                new Thread()
                {
                    @Override
                    public void run()
                    {
                        logger.info("shutdown hook engaged, terminating server gracefully");
                        webServer.shutdown();

                        final Long serverShutdownStartTime = System.currentTimeMillis();

                        while(webServer.isRunning() &&
                                (System.currentTimeMillis() - serverShutdownStartTime) < maxWaitForServerToTerminate)
                        {
                            try
                            {
                                Thread.currentThread().sleep(1000); // wait one second between test
                            }
                            catch (final Exception ex)
                            {
                                logger.info(
                                        "an exception occurred while waiting in shutdown hook. Exiting JVM.",
                                        ex);

                                System.exit(1);
                            }
                        }

                        if(webServer.isRunning())
                        {
                            logger.warn(
                                    String.format(
                                            "server termination taking more then expected time [%d] seconds. Force terminating JVM.",
                                            maxWaitForServerToTerminate / 1000));

                            System.exit(1);
                        }

                        logger.info("server was shutdown cleanly, exiting shutdown hook");
                    }
                });

        serverThread.join();
    }
}
