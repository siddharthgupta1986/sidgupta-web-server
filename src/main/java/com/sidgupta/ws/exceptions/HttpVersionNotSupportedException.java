package com.sidgupta.ws.exceptions;

/**
 * Created by sidgupta on 5/27/15.
 */
public class HttpVersionNotSupportedException extends Exception
{
    public HttpVersionNotSupportedException(final String httpVersion)
    {
        super(
                String.format(
                        "http version [%s] is not supported",
                        httpVersion));
    }
}
