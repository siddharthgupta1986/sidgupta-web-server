package com.sidgupta.ws.exceptions;

/**
 * Created by sidgupta on 5/26/15.
 */
public class HttpRequestNotYetSupportedException extends Exception
{
    public HttpRequestNotYetSupportedException(final String message)
    {
        super(message);
    }

    public HttpRequestNotYetSupportedException(final String formatString, final Object... params)
    {
        this(String.format(formatString, params));
    }
}
