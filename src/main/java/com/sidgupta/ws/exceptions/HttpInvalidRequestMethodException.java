package com.sidgupta.ws.exceptions;

import com.sidgupta.ws.utils.Constants;

/**
 * Created by sidgupta on 5/26/15.
 */
public class HttpInvalidRequestMethodException extends HttpInvalidRequestException
{
    public HttpInvalidRequestMethodException(final String methodName)
    {
        super(
                String.format(
                        "invalid or unknown request method [%s]",
                        (methodName == null)? Constants.NULL_STRING : methodName));
    }
}
