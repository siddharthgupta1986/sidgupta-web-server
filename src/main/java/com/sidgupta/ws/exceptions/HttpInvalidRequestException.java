package com.sidgupta.ws.exceptions;

/**
 * Created by sidgupta on 5/26/15.
 */
public class HttpInvalidRequestException extends Exception
{
    public HttpInvalidRequestException(final String message)
    {
        super(message);
    }

    public HttpInvalidRequestException(final String formatString, final Object ... params)
    {
        this(String.format(formatString, params));
    }
}
