package com.sidgupta.ws.http;

import com.sidgupta.ws.exceptions.HttpInvalidRequestException;
import com.sidgupta.ws.exceptions.HttpRequestNotYetSupportedException;
import com.sidgupta.ws.exceptions.HttpVersionNotSupportedException;
import com.sidgupta.ws.utils.Constants;
import com.sidgupta.ws.utils.Ensure;
import com.sidgupta.ws.utils.MapExtensions;
import com.sidgupta.ws.utils.StringExtensions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sidgupta on 5/26/15.
 */
public class HttpRequest
{
    private static final String REQUEST_LINE_SPLITTER = "\\s";
    private static final String HTTP_VERSION_SPLITTER = "\\.";

    public final HttpRequestMethod requestMethod;
    public final String resource;
    public final Map<String, String> requestHeaders;
    public final Map<String, String> queryParams;

    public HttpRequest(
            final HttpRequestMethod requestMethod,
            final String resource,
            final Map<String, String> requestHeaders,
            final Map<String, String> queryParams)
    {
        this.requestMethod = Ensure.notNull(requestMethod, "requestMethod");
        this.resource = Ensure.notNullOrEmpty(resource, "resource");

        if(MapExtensions.isNotNullOrEmpty(requestHeaders))
            this.requestHeaders = Collections.unmodifiableMap(requestHeaders);
        else
            this.requestHeaders = Collections.unmodifiableMap(new HashMap<String, String>());

        if(MapExtensions.isNotNullOrEmpty(queryParams))
            this.queryParams = Collections.unmodifiableMap(queryParams);
        else
            this.queryParams = Collections.unmodifiableMap(new HashMap<String, String>());
    }

    @Override
    public String toString()
    {
        final StringBuffer buffer = new StringBuffer();
        buffer.append(String.format("Request Method : [%s]\n", requestMethod.requestMethodName));
        buffer.append(String.format("Requested Resource : [%s]\n", resource));
        buffer.append(String.format("Request Headers : [%s]\n", requestHeaders.toString()));
        buffer.append(String.format("Query Params : [%s]\n", queryParams.toString()));

        return buffer.toString();
    }

    public static HttpRequest getFrom(final InputStream incomingRequestStream)
            throws IOException, HttpInvalidRequestException, HttpRequestNotYetSupportedException,
            HttpVersionNotSupportedException
    {
        Ensure.notNull(incomingRequestStream, "incomingRequestStream");

        final BufferedReader requestReader =
                new BufferedReader(
                        new InputStreamReader(
                                incomingRequestStream));

        final String requestLine = requestReader.readLine();

        if(StringExtensions.isNullOrEmpty(requestLine) || Character.isWhitespace(requestLine.charAt(0)))
            throw new HttpInvalidRequestException("invalid http request - request line invalid");

        final String[] requestType = requestLine.split(REQUEST_LINE_SPLITTER);
        if(requestType.length != 3)
            throw new HttpInvalidRequestException(
                    "invalid http request - request line should have 3 parts found [%s]",
                    requestType.length);

        final String requestHttpVersion = requestType[2];

        if(StringExtensions.isNullOrEmpty(requestHttpVersion))
            throw new HttpInvalidRequestException
                    ("invalid http request version [%s]",
                            (requestHttpVersion == null)?
                                    Constants.NULL_STRING :
                                    Constants.EMPTY_STRING);

        if(requestHttpVersion.indexOf("HTTP/") == 0 && requestHttpVersion.indexOf(Constants.PERIOD) > 5)
        {
            final String[] httpVersionRepresentation = requestHttpVersion.substring(5).split(HTTP_VERSION_SPLITTER);
            try
            {
                final int majorVersion = Integer.parseInt(httpVersionRepresentation[0]);
                final int minorVersion = Integer.parseInt(httpVersionRepresentation[1]);

                if(majorVersion != 1 && minorVersion != 1)
                    throw new HttpVersionNotSupportedException(requestHttpVersion);
            }
            catch (final NumberFormatException ex)
            {
                throw new HttpInvalidRequestException("invalid http request version [%s]", requestHttpVersion);
            }
        }
        else
        {
            throw new HttpInvalidRequestException("invalid http request version [%s]", requestHttpVersion);
        }

        final HttpRequestMethod requestMethod = HttpRequestMethod.getFrom(requestType[0]);

        if(requestMethod != HttpRequestMethod.GET)
        {
            throw new HttpRequestNotYetSupportedException(
                    "http request type [%s] not yet supported",
                    requestMethod.requestMethodName);
        }

        final String requestUri = requestType[1];

        final String resource;
        final Map<String, String> params;

        final int requestUrIParamsIndex = requestUri.indexOf(Constants.QUESTION_MARK);
        if(requestUrIParamsIndex < 0)
        {
            resource = URLDecoder.decode(requestUri, StandardCharsets.ISO_8859_1.name());
            params = null;
        }
        else
        {
            resource =
                    URLDecoder.decode(
                            requestUri.substring(0, requestUrIParamsIndex),
                            StandardCharsets.ISO_8859_1.name());

            final String paramsString = requestUri.substring(requestUrIParamsIndex+1);
            params = getParamsFrom(paramsString);
        }

        final Map<String, String> headers = getHeaders(requestReader);

        return new HttpRequest(requestMethod, resource, headers, params);
    }

    private static Map<String, String> getParamsFrom(final String paramsString) throws IOException
    {
        if(StringExtensions.isNullOrEmpty(paramsString))
            return null;

        final Map<String, String> queryParams = new HashMap<String, String>();

        final String[] params = paramsString.split(Constants.AMPERSAND);
        for(final String currentParam : params)
        {
            final String[] currentParamKeyValueSplit = currentParam.split(Constants.EQUALS);
            if(currentParamKeyValueSplit.length == 2)
            {
                queryParams.put(
                        URLDecoder.decode(currentParamKeyValueSplit[0], StandardCharsets.ISO_8859_1.name()),
                        URLDecoder.decode(currentParamKeyValueSplit[1], StandardCharsets.ISO_8859_1.name()));
            }
            else if(currentParamKeyValueSplit.length == 1 &&
                    (currentParam.indexOf(Constants.EQUALS) == currentParam.length() - 1))
            {
                queryParams.put(
                        URLDecoder.decode(currentParamKeyValueSplit[0], StandardCharsets.ISO_8859_1.name()),
                        Constants.EMPTY_STRING);
            }
        }

        return queryParams;
    }

    private static Map<String, String> getHeaders(final BufferedReader reader)
            throws IOException, HttpInvalidRequestException
    {
        final Map<String, String> headers = new HashMap<String, String>();

        String line;
        while(!(line = reader.readLine()).equals(Constants.EMPTY_STRING))
        {
            int separationIndex = line.indexOf(Constants.COLON);
            if(separationIndex <= 0)
            {
                throw new HttpInvalidRequestException("found invalid header");
            }
            else
            {
                headers.put(
                        line.substring(0, separationIndex).toLowerCase(),
                        line.substring(separationIndex+1).trim());
            }
        }

        return headers;
    }
}
