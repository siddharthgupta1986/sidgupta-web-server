package com.sidgupta.ws.http;

import com.sidgupta.ws.utils.Constants;
import com.sidgupta.ws.utils.Ensure;
import com.sidgupta.ws.utils.MapExtensions;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sidgupta on 5/27/15.
 */
public class HttpResponse
{
    public final HttpResponseStatus responseStatus;
    public final Map<String, String> headers;
    public final String mimeType;
    public final InputStream responseStream;

    public HttpResponse(
            final HttpResponseStatus responseStatus ,
            final Map<String, String> headers,
            final String mimeType,
            final InputStream responseStream)
    {
        this.responseStatus = Ensure.notNull(responseStatus, "responseStatus");

        if(MapExtensions.isNullOrEmpty(headers))
            this.headers = Collections.unmodifiableMap(new HashMap<String, String>());
        else
            this.headers = Collections.unmodifiableMap(headers);

        this.mimeType = mimeType;
        this.responseStream = responseStream;
    }

    public HttpResponse(
            final HttpResponseStatus responseStatus ,
            final String responseMessage)
    {
        this(
                responseStatus,
                new HashMap<String, String>()
                {{
                    put(Constants.CONTENT_LENGTH_HEADER_KEY, String.valueOf(responseMessage.length()));
                }},
                Constants.TEXT_PLAIN_MIME_TYPE,
                new ByteArrayInputStream(responseMessage.getBytes()));
    }
}
