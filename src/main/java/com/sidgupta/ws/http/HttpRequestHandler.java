package com.sidgupta.ws.http;

import java.util.function.Function;

/**
 * Created by sidgupta on 5/27/15.
 */
public interface HttpRequestHandler extends Function<HttpRequest, HttpResponse>
{
}
