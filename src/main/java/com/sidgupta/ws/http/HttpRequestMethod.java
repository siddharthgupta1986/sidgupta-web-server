package com.sidgupta.ws.http;

import com.sidgupta.ws.exceptions.HttpInvalidRequestMethodException;
import com.sidgupta.ws.utils.StringExtensions;

/**
 * Created by sidgupta on 5/26/15.
 */
public enum HttpRequestMethod
{
    GET("GET"),
    PUT("PUT"),
    POST("POST"),
    DELETE("DELETE"),
    OPTIONS("OPTIONS"),
    HEAD("HEAD"),
    TRACE("TRACE"),
    CONNECT("CONNECT");

    public final String requestMethodName;

    private HttpRequestMethod(final String requestMethodName)
    {
        this.requestMethodName = requestMethodName;
    }

    public static HttpRequestMethod getFrom(final String input) throws HttpInvalidRequestMethodException
    {
        if(StringExtensions.isNullOrEmpty(input))
            throw new HttpInvalidRequestMethodException(input);

        for(final HttpRequestMethod method : HttpRequestMethod.values())
            if(method.requestMethodName.equals(input))
                return method;

        throw new HttpInvalidRequestMethodException(input);
    }
}
