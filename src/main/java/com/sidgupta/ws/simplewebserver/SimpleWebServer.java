package com.sidgupta.ws.simplewebserver;

import com.sidgupta.ws.http.HttpRequestHandler;
import com.sidgupta.ws.utils.Ensure;
import com.sidgupta.ws.utils.ExceptionExtensions;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by siddharth on 5/26/15.
 */
public class SimpleWebServer implements Runnable
{
    private static final Logger logger = Logger.getLogger(SimpleWebServer.class);

    private final int serverPortNumber;
    private final int numRequestThreads;
    private final HttpRequestHandler requestHandler;
    private final ExecutorService requestHandlingExecutor;

    private final ServerSocket serverSocket;
    private boolean isShutdown = false;
    private boolean isRunning = false;

    public SimpleWebServer(
            final int serverPortNumber,
            final int numRequestThreads,
            final HttpRequestHandler requestHandler)
                throws IOException
    {
        this(
                serverPortNumber,
                numRequestThreads,
                requestHandler,
                Executors.newFixedThreadPool(numRequestThreads));
    }

    public SimpleWebServer(
            final int serverPortNumber,
            final int numRequestThreads,
            final HttpRequestHandler requestHandler,
            final ExecutorService requestHandlingExecutor)
            throws IOException
    {
        this.serverPortNumber = serverPortNumber;
        this.numRequestThreads = numRequestThreads;

        this.requestHandler = Ensure.notNull(requestHandler, "requestHandler");
        this.requestHandlingExecutor = Ensure.notNull(requestHandlingExecutor, "requestHandlingExecutor");

        this.serverSocket = new ServerSocket(this.serverPortNumber);
        this.serverSocket.setSoTimeout(2000); // 2 seconds max wait
    }

    private void safeTermiateRequestHandlingExecutor()
    {
        try
        {
            requestHandlingExecutor.shutdown();
            requestHandlingExecutor.awaitTermination(60, TimeUnit.SECONDS);
        }
        catch (final Exception ex)
        {
            System.out.println(ExceptionExtensions.getExceptionMessageWithStackTraceFor(ex));
        }
        finally
        {
            if(!requestHandlingExecutor.isTerminated())
                requestHandlingExecutor.shutdownNow();
        }
    }

    public void shutdown()
    {
        logger.info("server shutdown called");
        this.isShutdown = true;
    }

    public boolean isRunning()
    {
        return isRunning;
    }

    public void run()
    {
        isRunning = true;
        logger.info("starting web server");

        while(!Thread.currentThread().isInterrupted() && !isShutdown)
        {
            try
            {
                final Socket requestSocket = serverSocket.accept();

                final ServerRequestRunner requestRunner = new ServerRequestRunner(requestSocket, requestHandler);
                requestHandlingExecutor.submit(requestRunner);
            }
            catch(final SocketTimeoutException ex)
            {
                continue;
            }
            catch (final Exception ex)
            {
                logger.error("in server caught unhandeled exception. Terminating server", ex);
                shutdown();
            }
        }

        logger.info("server shutdown initiated");

        IOUtils.closeQuietly(serverSocket);
        safeTermiateRequestHandlingExecutor();

        logger.info("server shutdown completed");
        isRunning = false;
    }
}
