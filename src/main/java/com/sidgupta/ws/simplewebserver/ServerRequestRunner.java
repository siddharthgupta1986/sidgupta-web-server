package com.sidgupta.ws.simplewebserver;

import com.sidgupta.ws.exceptions.HttpInvalidRequestException;
import com.sidgupta.ws.exceptions.HttpRequestNotYetSupportedException;
import com.sidgupta.ws.exceptions.HttpVersionNotSupportedException;
import com.sidgupta.ws.http.HttpRequest;
import com.sidgupta.ws.http.HttpRequestHandler;
import com.sidgupta.ws.http.HttpResponse;
import com.sidgupta.ws.http.HttpResponseStatus;
import com.sidgupta.ws.utils.Constants;
import com.sidgupta.ws.utils.Ensure;
import com.sidgupta.ws.utils.StringExtensions;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Map;

/**
 * Created by siddharth on 5/26/15.
 */
public class ServerRequestRunner implements Runnable
{
    private static final Logger logger = Logger.getLogger(ServerRequestRunner.class);

    private final Socket requestSocket;
    private final HttpRequestHandler requestHandler;

    public ServerRequestRunner(final Socket requestSocket, final HttpRequestHandler requestHandler)
    {
        this.requestSocket = Ensure.notNull(requestSocket, "requestSocket");
        this.requestHandler = Ensure.notNull(requestHandler, "requestHandler");
    }

    public void run()
    {
        final String remoteHostName =
                (requestSocket.getRemoteSocketAddress() == null)?
                        Constants.NULL_STRING :
                        requestSocket.getRemoteSocketAddress().toString();

        HttpResponse response = null;
        try
        {
            requestSocket.setSoTimeout(30 * 1000); // 30 seconds

            final HttpRequest request =
                    HttpRequest.getFrom(requestSocket.getInputStream());

            logger.info(
                    String.format(
                            "[%s] for host [%s] serving request [%s]",
                            Thread.currentThread().getName(),
                            remoteHostName,
                            request.toString()));

            response = requestHandler.apply(request);
        }
        catch(final HttpInvalidRequestException ex)
        {
            response =
                    new HttpResponse(
                            HttpResponseStatus.BAD_REQUEST,
                            ex.getMessage());
        }
        catch(final HttpRequestNotYetSupportedException ex)
        {
            response =
                    new HttpResponse(
                            HttpResponseStatus.NOT_IMPLEMENTED,
                            ex.getMessage());
        }
        catch(final HttpVersionNotSupportedException ex)
        {
            response =
                    new HttpResponse(
                            HttpResponseStatus.HTTP_VERSION_NOT_SUPPORTED,
                            ex.getMessage());
        }
        catch(final SocketTimeoutException ex)
        {
            logger.info(
                    String.format(
                            "[%s] request for host [%s] caught request timeout exception. returning with [%s].",
                            Thread.currentThread().getName(),
                            remoteHostName,
                            HttpResponseStatus.REQUEST_TIMEOUT.toString()),
                    ex);

            response =
                    new HttpResponse(
                            HttpResponseStatus.REQUEST_TIMEOUT,
                            "timed out while waiting to read from the socket");
        }
        catch(final Exception ex)
        {
            logger.info(
                    String.format(
                            "[%s] request for host [%s] caught unhandeled exception. returning with [%s].",
                            Thread.currentThread().getName(),
                            remoteHostName,
                            HttpResponseStatus.INTERNAL_SERVER_ERROR.toString()),
                    ex);

            response =
                    new HttpResponse(
                            HttpResponseStatus.INTERNAL_SERVER_ERROR,
                            "server caught an internal error");
        }

        try
        {
            if(response == null)
            {
                logger.info(
                        String.format(
                                "[%s] while serving request for host [%s] an unknown internal error happened, " +
                                        "returning with [%s].",
                                Thread.currentThread().getName(),
                                remoteHostName,
                                HttpResponseStatus.INTERNAL_SERVER_ERROR.toString()));

                response =
                        new HttpResponse(
                                HttpResponseStatus.INTERNAL_SERVER_ERROR,
                                "unknown internal server error");
            }

            final OutputStream output = requestSocket.getOutputStream();
            final PrintWriter outputWriter =
                    new PrintWriter(
                            new OutputStreamWriter(
                                    output, StandardCharsets.UTF_8),
                            true /* autoFlush */);

            outputWriter.println(
                    String.format(
                            "%s %d %s",
                            Constants.HTTP_VERSION,
                            response.responseStatus.statusCode,
                            response.responseStatus.reason));

            outputWriter.println(
                    StringExtensions.getHeaderStringFrom(
                            Constants.DATE_HEADER_KEY,
                            new Date().toString()));

            outputWriter.println(
                    StringExtensions.getHeaderStringFrom(
                            Constants.SERVER_NAME_HEADER_KEY,
                            Constants.SERVER_NAME_HEADER_VALUE));

            outputWriter.println(
                    String.format(
                            "%s: %s; %s=%s",
                            Constants.CONTENT_TYPE_HEADER_KEY,
                            (response.mimeType == null)? Constants.EMPTY_STRING : response.mimeType,
                            Constants.CHARSET_HEADER_KEY,
                            StandardCharsets.UTF_8.toString()));

            for(final Map.Entry<String, String> entry : response.headers.entrySet())
                outputWriter.println(
                        StringExtensions.getHeaderStringFrom(
                                entry.getKey(),
                                entry.getValue()));

            // marking the end of headers
            outputWriter.println(Constants.EMPTY_STRING);

            if(response.responseStream != null)
                IOUtils.copy(response.responseStream, outputWriter, StandardCharsets.UTF_8);

            outputWriter.flush();
        }
        catch (final Exception ex)
        {
            logger.info(
                    String.format(
                            "[%s] while sending response to host [%s] caught exception. Ignoring.",
                            Thread.currentThread().getName(),
                            remoteHostName),
                    ex);
        }
        finally
        {
            IOUtils.closeQuietly(response.responseStream);
            IOUtils.closeQuietly(requestSocket);
        }
    }
}
