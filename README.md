SimpleWebServer - Author Siddharth Gupta
=======================================

Steps to run server:

0. Ensure that you have Java 8 SDK installed and JAVA_HOME is set to JDK8 directory
0. git clone https://siddharthgupta1986@bitbucket.org/siddharthgupta1986/sidgupta-web-server.git
0. cd sidgupta-web-server
0. mvn clean install, look for the jar path in your local repo. Make sure that you get the path containing trailing  *jar-with-dependencies.jar name.
0. run : java -jar <JAR_FILE_DIR>/SidguptaWebServer-1.0-SNAPSHOT-jar-with-dependencies.jar -p 8888 -d /Users/sidgupta/tmp/ -t 10

0. Usage helpfor the HttpServer:

```
usage: HttpServer
 -d,--server-directory <arg>     directory (string) from which the files
                                 will be served
 -p,--server-port <arg>          port number (int) on which server will
                                 listen
 -t,--num-server-threads <arg>   number (int) of server worker threads
```

0. If you are importing project in IntelliJ/(Other IDE) make sure that you set the Project Language Level to Java 8
0. There is a test http client class present by the name HttpTestetClient.java. You can configure here number of parallel threads and number of requests per thread to execute a load test.


Some notes/future-scope on the server:

0. Supports parsing of only HTTP GET currently.
0. More logging statements should be added in furture.
0. Log4j could be extended to log to file as well, currently we are logging only to console.
0. Add unit tests

